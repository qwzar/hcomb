package graph

import (
	context "context"
	"log"
	"time"

	model "gitlab.com/qwzar/hcomb/gateway/model"
)

func (r *queryResolver) Candy(ctx context.Context, id *int) (model.Candy, error) {
	ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	result, err := r.twixClient.GetCandy(ctx, int32(*id))

	if err != nil {
		log.Println(err)
	}

	return model.Candy{
		ID:   int(result.Id),
		Name: result.Name,
	}, nil
}
