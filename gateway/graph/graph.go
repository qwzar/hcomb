package graph

import (
	"gitlab.com/qwzar/hcomb/twix/cmd/client"
)

//Resolver Twirp RPC client struct
type Resolver struct {
	twixClient *twix.Client
}

func (r *Resolver) Mutation() MutationResolver {
	return &mutationResolver{r}
}
func (r *Resolver) Query() QueryResolver {
	return &queryResolver{r}
}

func New(twixURL string) Config {
	twixClient, _ := twix.NewClient(twixURL)
	c := Config{
		Resolvers: &Resolver{
			twixClient,
		},
	}
	return c
}

type mutationResolver struct{ *Resolver }

type queryResolver struct{ *Resolver }
