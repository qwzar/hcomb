package main

import (
	"context"
	"log"

	"github.com/machinebox/graphql"
)

type test struct {
	GetCategory struct {
		Name string `json:"name"`
	} `json:"getCategory"`
}

func main() {
	client := graphql.NewClient("http://api-dev.unie.kz/v1")
	// make a request
	req := graphql.NewRequest(`	
		query {
			getCategory(id: 1){name}
		}
	`)

	ctx := context.Background()

	var res test

	if err := client.Run(ctx, req, &res); err != nil {
		log.Fatal(err)
	}
	log.Println(res.GetCategory.Name)
}

// http://api-dev.unie.kz/v1?query={getCategory(id: 1){name id subcategories{id name }}}
