package twixserver

import (
	"context"
	"math/rand"

	"github.com/twitchtv/twirp"
	pb "gitlab.com/qwzar/hcomb/twix/rpc/twix"
)

// New server
func New() *server {
	return new(server)
}

type server struct{}

// GetCandy handler.
func (s *server) GetCandy(ctx context.Context, req *pb.GetCandyRequest) (res *pb.GetCandyResponse, err error) {
	if req.Id <= 0 {
		return nil, twirp.InvalidArgumentError("id", "I can't find candys")
	}
	return &pb.GetCandyResponse{
		Candy: &pb.Candy{
			Id:   req.Id,
			Name: []string{"left", "right", "halaopa"}[rand.Intn(3)],
		},
	}, nil
}

// GetCandys handler.
func (s *server) GetCandys(ctx context.Context, req *pb.GetCandysRequest) (res *pb.GetCandysResponse, err error) {

	candys := []*pb.Candy{}

	return &pb.GetCandysResponse{Candys: candys}, nil
}
