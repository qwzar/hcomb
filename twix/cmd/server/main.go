package main

import (
	"net/http"
	"os"

	"gitlab.com/qwzar/hcomb/twix/internal/hooks"
	"gitlab.com/qwzar/hcomb/twix/internal/twix"
	"gitlab.com/qwzar/hcomb/twix/rpc/twix"
)

func main() {
	hook := hooks.LoggerHooks(os.Stderr)
	server := twixserver.New()
	twirpHandler := twix.NewTwixServiceServer(server, hook)

	mux := http.NewServeMux()
	mux.Handle(twix.TwixServicePathPrefix, twirpHandler)
	mux.HandleFunc("/", healthz)

	http.ListenAndServe(":8080", mux)
}

func healthz(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}
